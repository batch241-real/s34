const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.get('/home', (request, response) => {
	
	response.send("Welcome to the homepage!");
});


let users = [{
	'username': 'admin123',
	'password': 'admin123'
	}
];
app.get('/users', (request, response) =>{
	
	response.send(users);
});

app.delete('/delete-user', (request,response) => {
	let message;

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){

			message = `User ${request.body.username} is deleted `;

			let index = users[i];
			users.splice(index, 1);
			
		} else{
			message = "User does not exist.";
		}

		response.send(message)
	}
});

app.listen(port, () => console.log(`Server running at port ${port}`));