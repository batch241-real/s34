// use the 'require' directive to load express module/package
const express = require("express");

// create an application express
const app = express();

// for our application server to rune, we need a port to listen to
const port = 3000;

// allow your app to read json data
app.use(express.json());

// allow your app to read other data types
// {express: true} - by applying option, it allows us to receive information in other data type
app.use(express.urlencoded({extended: true}));


// [SECTION] Routes
// Express has methods corresponding to each HTTP method

// GET
// This route expects to receive a GET request at URI '/greet'
app.get("/greet", (request, response) =>{
	
	// response.send - sends a response back to the client
	response.send("Hello from the /greet endpoint!");
})


// POST
app.post('/hello', (request,response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
});


// Simple registration form

let users = [];
app.post('/signup', (request, response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`)
	} else{
		response.send("Please input BOTH username and password")
	}
})


// Change password
app.patch('/change-password', (request, response) => {

	// create a variable to store the message to be sent back to the client
	let message;

	// if the username provided in the Postman and the username of the current object in the loop is the same
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){

			// change the password
			users[i].password == request.body.password;

			// message to be sent if password has been updated successfully
			message = `User ${request.body.username}'s password has been updated' `;
			break;
		} else{
			message = "User does not exist.";
		}
	}
	response.send(message);
})


// Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
